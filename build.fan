using build

class Build : BuildPod {
  
  new make() {
    podName = "tacWiki"
    summary = "My Awesome tacWiki Project"
    version = Version("1.0")

    meta = [
      "proj.name" : "tacWiki"
    ]
    
    depends = [
      "sys 1.0",
      "util 1.0",
      "web 1.0",
      "jcWeb 1.0",
    ]
    
    srcDirs = [`fan/`, `fan/wikiBot/`]
    resDirs = [,]
    
    docApi = true
    docSrc = true
  }
}
