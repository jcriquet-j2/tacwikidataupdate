using util

const class MasterParam : JsonGameFile {
  const LocGameFile[] loc := LocGameFile.makeFor(LocGameFile#, [`LocalizedMasterParam`])
  const LocGameFile jpConceptCardLoc := LocGameFile("japanese", `external_conceptcard`)
  
  new make() : super(`MasterParam`) {}
  
  override Obj? parse(Uri remotePath, Str server) {
    toReturn := ["data": super.parse(remotePath, server)]
    return server == "gl"
      ? toReturn.set("loc", Str:LocGameFile[:].addList(loc) { lang }.map { it.parse(remotePath, server) })
      : toReturn.set("loc", ["japanese": Str:Str[:].with |m| {
        jpConceptCardLoc.parse(remotePath, server)->each |Str v, Str k| { m.set("SRPG_ConceptCardParam_$k", v) }
      }])
  }
  
  override Uri:Str generate(Str:Obj? data) {
    toReturn := Uri:Str[:]
    
    // Extract loc
    data = subDivide(data) |v, Str k| { k }
    Str:[Str:Str] loc := data["loc"]->get("gl")->addAll(data["loc"]->get("jp"))
    data = data["data"]
    getLocFor := |Str key, Str? jpVal->Str:Str| {
      m := loc.map |m| { m.remove(key) }.findAll { it != null }
      if (jpVal != null) m["japanese"] = jpVal
      return m
    }
    
    // Group by key
    groups := subDivide(data) |v, Str k| { k }
    
    getIname := |Str:Obj? m->Str| { m["iname"] }
    storePacket := |Str type, Str:Str locKeys, Str:Obj? packet, Str k| {
      locKeys.each |dataN, locN| {
        jpName := packet["jp"]?->get(dataN) ?: packet["gl"]?->get(dataN)
        packet.each |subpacket| { subpacket->remove(dataN) }
        packet.getOrAdd("loc") { Str:Str[:] }->set(dataN, getLocFor("SRPG_${type}Param_${k}_$locN", jpName))
      }
      |Obj?->Obj?|? recF
      recF = |Obj? obj->Obj?| {
        if (obj is Map) return ((Map) obj).map(recF)
        if (obj is List) return ((List) obj).map(recF)
        if (obj is Str) return mwSafeStr(obj)
        return obj
      }
      toReturn["Data:Game/MasterParam/$type/".toUri + k.toUri] = StrBuf()
        .add("{{#invoke:Data|insert")
        .add(packet.keys.rw.sort.map |server| { "|$server=" + mwSafe(JsonOutStream.writeJsonToStr(recF(packet[server]))) }.join)
        .add("}}")
        .toStr
    }
    [
      "Unit": ["NAME": "name"],
      "Job": ["NAME": "name", "CHARADESC": "desc_ch", "OTHERDESC": "desc_ot"],
      "JobSet": Str:Str[:],
      "Ability": ["NAME": "name", "EXPR": "expr"],
      "Buff": Str:Str[:],
      "Cond": Str:Str[:],
      "Artifact": ["NAME": "name", "EXPR": "expr", "SPEC": "spec", "FLAVOR": "flavor"],
      "Item": ["NAME": "name", "EXPR": "expr", "FLAVOR": "flavor"],
    ].each |locKeys, type| { subDivide(groups.remove(type), getIname).each(storePacket.bind([type, locKeys])) }
    
    // ConceptCard
    trustRewards := subDivide(groups.remove("ConceptCardTrustReward"), getIname)
    subDivide(groups.remove("ConceptCard"), getIname).each |packet, k| {
      packet.each |Str:Obj? p, Str server| {
        if (p["trust_reward"] == null) return
        [Str:Obj?]? trustReward := trustRewards[p.remove("trust_reward")]
        if (trustReward == null) return
        rewardsParsed := Int:[Str:Int][:] { ordered = true }
        trustReward[server]->get("rewards")->each |Str:Obj? reward| {
          switch (reward["reward_type"]) {
            case 1:
            case 2:
              m := rewardsParsed.getOrAdd(reward["reward_type"]) { [:] { ordered = true } }
              m[reward["reward_iname"]] = m.get(reward["reward_iname"], 0).plus(reward["reward_num"])
            default: throw Err("Encountered unimplemented trust reward type: $reward")
          }
        }
        [null, "item", "artifact"].each |s, i| {
          if (s != null)
            p.add("trust_${s}_names", rewardsParsed[i]?.keys ?: [,])
             .add("trust_${s}_counts", rewardsParsed[i]?.vals ?: [,])
        }
      }
      storePacket("ConceptCard", ["NAME": "name", "EXPR": "expr", "FLAVOR": "flavor"], packet, k)
    }
    
    // Skill
    weapons := subDivide(groups.remove("Weapon"), getIname)
    subDivide(groups.remove("Skill"), getIname).each |packet, k| {
      packet.each |Str:Obj? p, Str server| {
        if (p["weapon"] == null) return
        [Str:Obj?]? weapon := weapons[p.remove("weapon")]
        weapon = weapon?.get(server)
        if (weapon == null) return
        p.add("dmg_formula", weapon["formula"])
         .add("dmg_atk", weapon["atk"])
      }
      storePacket("Skill", ["NAME": "name", "EXPR": "expr", "MAPEFF": "me_desc"], packet, k)
    }
    
    // Recipes
    subDivide(groups.remove("Recipe"), getIname).each |packet, k| {
      packet.each |Str:Obj? p, Str server| {
        p["mats"] = (1..5).map { p.remove("mat$it") }
        p["counts"] = (1..5).map { p.remove("num$it") }
      }
      storePacket("Recipe", [:], packet, k)
    }
    
    return toReturn
  }
  
  Str:[Str:Obj?] subDivide(Str:Obj? serverMap, |Obj? v, Obj k->Str| f) {
    groups := Str:[Str:Obj?][:]
    serverMap.each |d, server| { d->each |Obj? v, Obj k| { groups.getOrAdd(f(v, k)) { Str:Obj?[:] }[server] = v } }
    return groups
  }
  
  private Str mwSafe(Str s) {
    s.replace("{", "{{(")
     .replace("}", ")}}")
     .replace("{{(", "{{(}}")
     .replace(")}}", "{{)}}")
     .replace("|", "{{!}}")
     .replace("\\\\u005", "\\u005")
  }
  
  private Str mwSafeStr(Str s) {
    s.replace("[", "\\u005b")
     .replace("]", "\\u005d")
  }
}
