
class LuaOutStream : OutStream {
  static const Regex reName := Regex("[a-zA-Z_][a-zA-Z0-9_]*")
  static const Str[] reservedNames := ["and","break","do","else","elseif","end","false","for","function","if","in","local","nil","not","or","repeat","return","then","true","until","while"]
  
  new make(OutStream out) : super(out) {}
  
  This writeLua(Obj? obj) {
         if (obj is Str)  writeLuaStr(obj)
    else if (obj is Num)  writeLuaNum(obj)
    else if (obj is Bool) writeLuaBool(obj)
    else if (obj is Map)  writeLuaMap(obj)
    else if (obj is List) writeLuaList(obj)
    else if (obj == null) writeLuaNull
    else throw Err("Unknown encoding: $obj.typeof")//writeLuaObj(obj)
    return this
  }
//  
//  private Void writeLuaObj(Obj obj)
//  {
//    type := Type.of(obj)
//
//    // if a simple, write it as a string
//    ser := type.facet(Serializable#, false) as Serializable
//    if (ser == null) throw IOErr("Object type not serializable: $type")
//
//    if (ser.simple)
//    {
//      writeJsonStr(obj.toStr)
//      return
//    }
//
//    // serialize as JSON object
//    writeChar(JsonToken.objectStart)
//    first := true
//    type.fields.each |f, i|
//    {
//      if (f.isStatic || f.hasFacet(Transient#) == true) return
//      if (first) first = false
//      else writeChar(JsonToken.comma)
//      writeJsonPair(f.name, f.get(obj))
//    }
//    writeChar(JsonToken.objectEnd)
//  }
  
  private Void writeLuaMap(Map map) {
    writeChar('{')
    notFirst := false
    map.each |val, key|
    {
      if (notFirst) writeChar(',')
      validName := key is Str && !reservedNames.contains(key) && reName.matches(key)
      if (validName) print(key)
      else writeChar('[').writeLua(key).writeChar(']')
      writeChar('=')
      writeLua(val)
      notFirst = true
    }
    writeChar('}')
  }
  
  private Void writeLuaList(List list) {
    writeChar('{')
    notFirst := false
    list.each |item|
    {
      if (notFirst) writeChar(',')
      writeLua(item)
      notFirst = true
    }
    writeChar('}')
  }
  
  private Void writeLuaStr(Str str) {
    writeChar('\'')
    str.each |char|
    {
      /*if (char <= 0x7f) */switch (char) {
        case '\b': writeChar('\\').writeChar('b')
        case '\f': writeChar('\\').writeChar('f')
        case '\n': writeChar('\\').writeChar('n')
        case '\r': writeChar('\\').writeChar('r')
        case '\t': writeChar('\\').writeChar('t')
        case '\\': writeChar('\\').writeChar('\\')
        case '"':  writeChar('\\').writeChar('"')
        case '\'':  writeChar('\\').writeChar('\'')
        default: writeChar(char)
      }
      //else writeChar('\\').print(char)
    }
    writeChar('\'')
  }
  
  private Void writeLuaNum(Num num) { print(num) }
  private Void writeLuaBool(Bool bool) { print(bool) }
  private Void writeLuaNull() { print("nil") }
}
