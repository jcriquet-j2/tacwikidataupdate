
class Main {
  WikiBot wikiBot := WikiBot(`https://thealchemistcode.gamepedia.com/`)
  
  Void main() {
    curDir := `./`.toFile
    GameFile.gameFiles.each |gameFile| {
      try {
        wikiFiles := gameFile.generate([
          "jp": `Japan/`,
          "gl": `Global/`,
        ].map |remotePath, server| { gameFile.parse(remotePath, server) })
        echo("Getting list of every existing wiki page")
        
        // Get all pages, which we will reduce to get pages to be deleted
        Str[] allPages := wikiBot.batchReq("query", "apcontinue", [
          "list": "allpages",
          "apnamespace": "10000",
          "aplimit": "max",
        ]) { it["query"]?->get("allpages") }.map { it->get("title") }
        
        echo("Comparing to $allPages.size existing wiki pages for $gameFile.typeof.name")
        // Get Failed Inserts
        Str[] failedInserts := wikiBot.batchReq("query", "cmcontinue", [
          "list": "categorymembers",
          "cmtitle": "Category:Data_pages_that_failed_a_table_insert",
          "cmlimit": "max",
        ]) { it["query"]?->get("categorymembers") }.map { it->get("title") }
        //failedInserts.clear
        
        while (!wikiFiles.isEmpty)
          wikiBot.edit(wikiFiles, |uri| { echo("Updated $uri") }, allPages, failedInserts)
        
        // Delete unused pages
        x := true
        allPages.exclude {
          split('/').size != 4 ||
          "Ability,Item,Unit".split(',').contains(split('/')[2]).not
        }.each {
          y := wikiBot.apiReq("delete", [:], [
            "title": it,
            "reason": "Removed from Data",
            "watchlist": "unwatch",
            "token": wikiBot.csrfToken,
          ])
          echo("Deleted page $it")
          if (x) {
            echo(y)
            x = false
          }
        }
        echo(allPages.findAll {
          split('/').size != 4 ||
          "Ability,Item,Unit,Skill".split(',').contains(split('/')[2]).not
        }.sort.join("\n"))
      } catch (Err e) { e.trace }
      echo("Wiki updating complete!")
    }
  }
}

class Asdf {
  static Obj? parseObj(Obj? obj) {
    if (obj == null) return null
    if (obj is Str) return obj->startsWith("RECIPE_") ? obj : null
    if (obj is Map) return parseMap(obj)
    if (obj is List) return parseList(obj)
    return null
  }
  
  static [Str:Obj]? parseMap(Str:Obj? map) {
    m := map.map(Asdf#parseObj.func).findAll |v| { v != null }
    return m.isEmpty ? null : m
  }
  
  static Obj[]? parseList(Obj?[] list) {
    l := list.map(Asdf#parseObj.func).findAll |v| { v != null }
    if (l.first is Map)
      l = [l.reduce([:] { ordered = true }) |Str:Obj? acc, Str:Obj? val->Str:Obj?| { acc.setAll(val) }]
    return l.isEmpty ? null : l
  }
}
