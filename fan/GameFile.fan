using util
using jcWeb

abstract const class GameFile {
  static const HttpCache cache := HttpCache(`./`.toFile)
  
  static const GameFile[] gameFiles := Pod.find("tacWiki").types.map |t| {
    try {
    if (!t.fits(GameFile#)) return [,]
    f := t.field("val", false)
    if (f?.isStatic == true && f.type.fits(GameFile#)) return [f.get]
    f = t.field("vals", false)
    if (f?.isStatic == true && f.type.fits(GameFile[]#)) return f.get
    if (t.method("make", false).params.all |p| { p.hasDefault }) return [t.make]
    return [,]
      
    } catch (Err e) {
      e.trace
      throw e
    }
  }.flatten
  
  const Uri endPoint
  new make(Uri endPoint) { this.endPoint = endPoint }
  virtual Obj? parse(Uri remotePath, Str server) {
    file := HttpFile(`https://gitlab.com/the-alchemist-codes/` + remotePath + `raw/master/` + endPoint, cache)
    echo("Downloading $file.uri")
    in := file.in
    try {
      gObj := _parse(in)
      if (server == "jp") echo(Asdf.parseObj(gObj))
      echo("Successfully parsed: ($server) $endPoint")
      in.close
      cache.save
      return gObj
    } finally in.close
  }
  abstract Obj? _parse(InStream in)
  abstract Uri:Str generate(Str:Obj? data)
}

abstract const class JsonGameFile : GameFile {
  new make(Uri endPoint) : super(`Data/` + endPoint) {}
  override Obj? _parse(InStream in) { JsonInStream(in).readJson }
}

const class LocGameFile : GameFile {
  static LocGameFile[] makeFor(Type t, Obj?[] args := [,]) { [
    "english",
    "french",
    "german",
    "spanish",
  ].map { t.make(Obj?[it].addAll(args)) } }
  const Str lang
  new make(Str lang, Uri endPoint) : super(`Loc/` + "$lang/".toUri + endPoint) { this.lang = lang }
  override Obj? _parse(InStream in) {
    in.read; in.read; in.read // BOM
    if (in.peek == null) throw Err("Empty file")
    map := Str:Str[:]
    in.readAllLines.each {
      strs := split('\t')
      map[strs.first] = strs.last
    }
    return map
  }
  override Uri:Str generate(Str:Obj? data) { [:] }
}

